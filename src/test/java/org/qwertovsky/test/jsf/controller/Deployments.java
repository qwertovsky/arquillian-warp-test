package org.qwertovsky.test.jsf.controller;

import java.io.File;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;

public class Deployments {
	
	
	public static WebArchive createDeployment() {
		WebArchive war = ShrinkWrap.createFromZipFile(WebArchive.class
				, new File("target/arquillian-warp.war"));
		return war;
	}
	

}
