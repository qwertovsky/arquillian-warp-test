package org.qwertovsky.test.jsf.controller;

import java.net.URL;

import javax.faces.bean.ManagedProperty;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.warp.Activity;
import org.jboss.arquillian.warp.Inspection;
import org.jboss.arquillian.warp.Warp;
import org.jboss.arquillian.warp.WarpTest;
import org.jboss.arquillian.warp.jsf.BeforePhase;
import org.jboss.arquillian.warp.jsf.Phase;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@RunWith(Arquillian.class)
@WarpTest
@RunAsClient
public class IndexTest {
	
	@Drone
    private WebDriver browser;
	@ArquillianResource
    private URL deploymentUrl;
	
	@FindBy(id="form:member_name")                                     
	private WebElement memberName;
	@FindBy(id="form:add_member_btn")
	private WebElement memberAddBtn;
	
	@Deployment(testable=true)
	public static WebArchive createDeployment() {
		return Deployments.createDeployment();
	}
	
	
	
	@Test
	public void testEmptyMembers() {
		
		Warp.initiate(new Activity() {
			@Override
			public void perform() {
				System.out.println(deploymentUrl + "index.xhtml");
				browser.navigate().to(deploymentUrl + "index.xhtml");
			}
		}).inspect(new Inspection(){
			private static final long serialVersionUID = 1L;
			
			@ManagedProperty("#{memberController}")
			MemberController memberController;
			
			@BeforePhase(Phase.RESTORE_VIEW)
			public void inspectEmptyList() {
				System.out.println(memberController.getMembers().size());
				Assert.assertEquals(0, memberController.getMembers().size());
			}
		});
		
	}

}
